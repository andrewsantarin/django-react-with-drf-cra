# django-react-with-drf-cra
An example of running Django REST Framework with a frontend dashboard SPA generated with Create React App

----
## Premise

The idea is to be able to integrate a single-page app within the Django REST Framework project. The SPA can be anything:

- It can be a drop-in CRUD replacement for the usual Django admin site and any API documentation generator you may choose to use to aid developers in CRUD operations.
- It can be an actual app which your end users will access as a real application consuming the REST API; the app will act as a customized UI based on your end users' workflows.

----
## Installation

1. Obtain a .zip file copy from the repository downloads, or clone the repository using this command

    ```bash
    git clone https://andrewsantarin@bitbucket.org/andrewsantarin/django-react-with-drf-cra.git
    ```

2. Navigate into the directory.

    ```bash
    cd django-react-with-drf-cra
    ```

3. Install the dependencies.

    ```bash
    pip install -r requirements.txt
    ```
