const WebpackBundleTracker = require('webpack-bundle-tracker');

module.exports = {
  webpack: (config, env) => {
    config.plugins.push(
      new WebpackBundleTracker({
        path: __dirname,
        filename: './build/webpack-stats.json',
      }),
    );

    config.optimization.splitChunks.name = 'vendor';

    return config;
  },
};
